let number1, number2;
do{
    number1 = prompt("Будь ласка, введіть перше число:");
    number1 = parseFloat(number1);
} while (isNaN(number1));

do {
    number2 = prompt("Будь ласка, введіть друге число:");
    number2 = parseFloat(number2);
} while (isNaN(number2));

let min = Math.min(number1, number2);
let max = Math.max(number1, number2);

for (let i = min; i <= max; i++) {
    console.log(i);
}


let number = parseInt(prompt("Будь ласка, введіть число:"));

while (number % 2 !== 0) {
    number = parseInt(prompt("Введене число не є парним. Будь ласка, введіть парне число:"));
}

alert("Ви ввели парне число: " + number);